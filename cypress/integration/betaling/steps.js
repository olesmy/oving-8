// Gjennnomføre vellykket kjøp:
Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
  cy.visit('http://localhost:8080');

  cy.get('#product').select('Hubba bubba');
  cy.get('#quantity').clear().type('4');
  cy.get('#saveItem').click();

  cy.get('#product').select('Smørbukk');
  cy.get('#quantity').clear().type('5');
  cy.get('#saveItem').click();

  cy.get('#product').select('Stratos');
  cy.get('#quantity').clear().type('1');
  cy.get('#saveItem').click();

  cy.get('#product').select('Hobby');
  cy.get('#quantity').clear().type('2');
  cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, () => {
  cy.get('#goToPayment').click();
});

When(
  /^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/,
  () => {
    cy.get('#fullName').clear().type('Ole Olsen').blur();
    cy.get('#address').clear().type('Holenkollen 123').blur();
    cy.get('#postCode').clear().type('5555').blur();
    cy.get('#city').clear().type('Oslo').blur();
    cy.get('#creditCardNo').clear().type('1122334455667788').blur();
  }
);

And(/^trykker på Fullfør kjøp$/, () => {
  cy.get('.formField').contains('Fullfør handel').click();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
  cy.get('.confirmation').contains('Din ordre er registrert.');
});

// Misslykket kjøp:

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
  cy.visit('http://localhost:8080');

  cy.get('#product').select('Hubba bubba');
  cy.get('#quantity').clear().type('4');
  cy.get('#saveItem').click();

  cy.get('#product').select('Smørbukk');
  cy.get('#quantity').clear().type('5');
  cy.get('#saveItem').click();

  cy.get('#product').select('Stratos');
  cy.get('#quantity').clear().type('1');
  cy.get('#saveItem').click();

  cy.get('#product').select('Hobby');
  cy.get('#quantity').clear().type('2');
  cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, () => {
  cy.get('#goToPayment').click();
});

// (Litt usikker på herfra og ned:)
When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
  cy.get('#fullName').clear().blur();
  cy.get('#address').clear().blur();
  cy.get('#postCode').clear().blur();
  cy.get('#city').clear().blur();
  cy.get('#creditCardNo').clear().blur();
});

Then(/^skal jeg få feilmeldinger for disse$/, () => {
  cy.get('.formField').contains('Feltet må ha en verdi');
  cy.get('.formField').contains('Kredittkortnummeret må bestå av 16 siffer');
});
